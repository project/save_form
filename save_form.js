/**
 * @file
 * JS file of save_form, to auto save the forms
 */


/**
 * Using Drupal behaviours to declare main function
 */
Drupal.behaviors.save_form = function() {
  $.each(Drupal.settings['save_form'], function(autosave_form, saving_now) {
    var form = $(autosave_form);

    // Only trigger the save X seconds after a change.
    $(':input[type!="submit"]', form.get(0)).live ('change', function (e) {
      // /* DEBUG */ console.log('changed');
      if (Drupal.settings['save_form'][autosave_form]) {
        Drupal.settings['save_form'][autosave_form] = false;
        setTimeout(
          // 'console.log("' + autosave_form + ' is DIRTY! ' + form.attr('action') +'"); ' +
            'Drupal.settings["save_form"]["' + autosave_form + '"] = true; '
            + '$.post("' + form.attr('action') + '", $("' + autosave_form + '").serialize() + "&op=' + $('#edit-save-form', form).attr('value') + '")'
                    , Drupal.settings['save_form_timeout'] * 1000);
      }
    });
   
  });
};